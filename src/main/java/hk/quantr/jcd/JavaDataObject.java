package hk.quantr.jcd;

import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.NbBundle.Messages;

@Messages({
	"LBL_Java_LOADER=Files of Java"
})
@MIMEResolver.ExtensionRegistration(
		displayName = "#LBL_Java_LOADER",
		mimeType = "text/x-java",
		extension = {"java"}
)
@DataObject.Registration(
		mimeType = "text/x-java",
		iconBase = "hk/quantr/jcd/javaLogo16_16.png",
		displayName = "#LBL_Java_LOADER",
		position = 300
)
//@ActionReferences({
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
//			position = 100,
//			separatorAfter = 200
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
//			position = 300
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
//			position = 400,
//			separatorAfter = 500
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
//			position = 600
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
//			position = 700,
//			separatorAfter = 800
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
//			position = 900,
//			separatorAfter = 1000
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
//			position = 1100,
//			separatorAfter = 1200
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
//			position = 1300
//	),
//	@ActionReference(
//			path = "Loaders/text/x-java/Actions",
//			id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
//			position = 1400
//	)
//})
public class JavaDataObject extends MultiDataObject {

	public JavaDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
		super(pf, loader);
		registerEditor("text/x-java", true);
	}

	@Override
	protected int associateLookup() {
		return 1;
	}

//	@MultiViewElement.Registration(
//			displayName = "#LBL_Java_EDITOR",
////			iconBase = "hk/quantr/jcd/javaLogo16_16.png",
//			mimeType = "text/x-java",
//			persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
//			preferredID = "Java",
//			position = 1000
//	)
//	@Messages("LBL_Java_EDITOR=Source2")
//	public static MultiViewEditorElement createEditor(Lookup lkp) {
//		return null;
//	}

}
